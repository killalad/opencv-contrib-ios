# opencv-contrib

[![CI Status](https://img.shields.io/travis/12817515/opencv-contrib.svg?style=flat)](https://travis-ci.org/12817515/opencv-contrib)
[![Version](https://img.shields.io/cocoapods/v/opencv-contrib.svg?style=flat)](https://cocoapods.org/pods/opencv-contrib)
[![License](https://img.shields.io/cocoapods/l/opencv-contrib.svg?style=flat)](https://cocoapods.org/pods/opencv-contrib)
[![Platform](https://img.shields.io/cocoapods/p/opencv-contrib.svg?style=flat)](https://cocoapods.org/pods/opencv-contrib)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

opencv-contrib is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'opencv-contrib'
```

## Author

12817515, martin.georgiu@gmail.com

## License

opencv-contrib is available under the MIT license. See the LICENSE file for more info.
